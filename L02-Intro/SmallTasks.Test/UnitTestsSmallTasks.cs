using NUnit.Framework;
using static SmallTasks.Task;

namespace SmallTasks.Test
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void IsOddReturnsTrueWhenNumberIsOdd()
        {
            Assert.That(IsOdd(3), Is.True);
        }

        [Test]
        public void IsOddReturnsFalseWhenNumberIsNotOdd()
        {
            Assert.That(IsOdd(2), Is.False);
        }

        [Test]
        public void IsPrimeReturnsTrueWhenNumberIsPrime()
        {
            Assert.That(IsPrime(3), Is.True);
        }

        [Test]
        public void IsPrimeReturnsFalseWhenNumberIsNotPrime()
        {
            Assert.That(IsPrime(4), Is.False);
        }

        [Test]
        public void MinWorksCorrectWithPositiveNumbers()
        {
            int[] arr = { 3, 1, 2 };

            Assert.That(Min(arr), Is.EqualTo(1));
        }

        [Test]
        public void MinWorksCorrectWithNegativeNumbers()
        {
            int[] arr = { -1, -3, -2 };

            Assert.That(Min(arr), Is.EqualTo(-3));
        }

        [Test]
        public void MinWorksCorrectWithDifferentNumbers()
        {
            int[] arr = { 1, 0, -2 };

            Assert.That(Min(arr), Is.EqualTo(-2));
        }

        [TestCase(-1)]
        [TestCase(4)]
        public void KthMinThrowsExceptionWhenPositionIsOutOfRange(int k)
        {
            int[] arr = { 3, 1, 2 };

            Assert.Throws<IndexOutOfRangeException>(() => KthMin(k, arr));
        }

        [Test]
        public void KthMinWorksCorrectWhenPositionIsInRange()
        {
            int k = 2;
            int[] arr = { 3, 1, 2 };

            Assert.That(KthMin(k, arr), Is.EqualTo(2));
        }

        [Test]
        public void GetOddOccurrenceWorksCorrectWhenOneOddCountExists()
        {
            int[] arr = { 1, 2, 1 };

            Assert.That(GetOddOccurrence(arr), Is.EqualTo(2));
        }

        [Test]
        public void GetOddOccurrenceWorksCorrectWhenSomeOddCountsExist()
        {
            int[] arr = { 1, 2, 3, 3, 1, 3 };

            Assert.That(GetOddOccurrence(arr), Is.EqualTo(2));
        }

        [Test]
        public void GetOddOccurrenceReturns_1WhenOddCountsDoNotExist()
        {
            int[] arr = { 1, 2, 2, 1 };

            Assert.That(GetOddOccurrence(arr), Is.EqualTo(-1));
        }

        [Test]
        public void GetAverageWorksCorrectWithPositiveNumbers()
        {
            int[] arr = { 1, 2, 3 };

            Assert.That(GetAverage(arr), Is.EqualTo(2));
        }

        [Test]
        public void GetAverageWorksCorrectWithNegativeNumbers()
        {
            int[] arr = { -1, -2, -3 };

            Assert.That(GetAverage(arr), Is.EqualTo(-2));
        }

        [Test]
        public void GetAverageWorksCorrectWithAllNumbers()
        {
            int[] arr = { 1, -2, 0 , -1};

            Assert.That(GetAverage(arr), Is.EqualTo(-0.5));
        }
    }
}