﻿using System;

namespace SmallTasks
{
    public class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Task
    {
        public static bool IsOdd(int n)
        {
            if (n % 2 == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsPrime(int n)
        {
            bool isPrime = true;

            for (int i = 2; i < n; i++)
            {
                if (n % i == 0)
                {
                    isPrime = false;
                    break;
                }
            }

            return isPrime;
        }

        public static int Min(int[] numbers)
        {
            //Array.Sort(numbers);
            //return numbers[0];

            int minNumber = int.MaxValue;

            foreach (int number in numbers) 
            {
                if (minNumber >= number)
                {
                    minNumber = number;
                }
            }

            return minNumber;
        }

        public static int KthMin(int k, int[] numbers)
        {
            if (k < 0 || k >= numbers.Length)
            {
                throw new IndexOutOfRangeException();
            }

            Array.Sort(numbers);
            return numbers[k - 1];
        }

        public static int GetOddOccurrence(int[] numbers)
        {
            Dictionary<int, int> counts = new Dictionary<int, int>();

            foreach (int number in numbers)
            {
                if (counts.ContainsKey(number))
                {
                    counts[number]++;
                }
                else
                {
                    counts[number] = 1;
                }
            }

            var oddCounts = counts.Where(x => x.Value % 2 == 1).Select(x => x.Key).ToArray();

            if (oddCounts.Length > 0)
            {
                return oddCounts.First();
            }
            else
            {
                Console.WriteLine("No such number");
                return -1;
            }
        }

        public static double GetAverage(int[] numbers)
        {
            //return numbers.Average();

            int sum = 0;
            foreach (int number in numbers)
            {
                sum += number; 
            }

            double average = sum*1.0/numbers.Length;

            return average;
        }

        public static long Pow(int a, int b)
        {
            return (long)Math.Pow(a, b);
        }
    }
}